FROM node:10

WORKDIR /app

COPY . .

RUN npm install --quiet

EXPOSE $PORT

CMD ["npm", "start"]
