const helloWorldController = require('../controllers/hello-world.controller')

module.exports = [
  {
    method: 'GET',
    path: '/',
    handler: helloWorldController.index
  }
]
