require('dotenv').config()

const Hapi = require('hapi')

const routes = require('./routes')

const server = Hapi.server({
  port: process.env.PORT
})

server.route(routes)

const init = async () => {
  await server.start()
  console.log(`Server started at: ${server.info.uri}`)
}

process.on('unhandledRejection', (err) => {
  console.log(err)
  process.exit(1)
})

init()
